function RSTparameter = adapt_controller_v1( sysParam )
%ADAPT_CONTROLLER Adapt an RST controller according to the current system
%parameter and the design requirements.
%   expect
%   sysParam: a1, a2, b1, b2
Amz = [1 -1.809674836071919 0.818730753077982];
Bmz = [0 3.846833925345054e-06 3.907036972632209e-05 3.606640703744371e-05 3.026022131584880e-06];
a1 = sysParam(1);
a2 = sysParam(2);
b1 = sysParam(3);
b2 = sysParam(4);
A = [ 1, 0, 0, 0, 0; ...
      -1+Amz(1+1), 1, 0, 0, 0; ...
      -a1+a2, -1+a1, b2, b1, 0; ...
      -a2, -a1+a2, 0, b2, b1; ...
      0, -a2, 0, 0, b2];
b = [0; -b1; 1; Amz(1+1); Amz(2+1)];
RS = A\b; %inv(A)*b

T0 = 0;
T1 = (Bmz(0+2)-b1*(Bmz(1+2)/b1))/(b1*b2);
T2 = Bmz(1+2)/b1;
r1 = RS(1);
r2 = RS(2);
R0 = r1;
R1 = r2-r1;
R2 = -r2;
S0 = RS(3);
S1 = RS(4);
S2 = RS(5);
RSTparameter.R = [R0 R1 R2];
RSTparameter.S = [S0 S1 S2];
RSTparameter.T = [T0 T1 T2];
end

