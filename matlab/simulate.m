clear all;
clc

%% INIT:
h=0.1; %[s]
simulation_time = 100; %[s]
t = (0:h:simulation_time)';
u = zeros(simulation_time/h+1, 1);
u(1:20/h-1)=300;
u(20/h: 40/h)=900;
u(40/h: 60/h-1)=1200;
u(60/h: 80/h-1)=600;
u(80/h: 100/h)=0;
TRr = zeros(simulation_time/h+1, 1);
e = zeros(simulation_time/h+1, 1);
f = zeros(simulation_time/h+1, 1); %feedback: S/T*y
y = zeros(simulation_time/h+1, 1);
Theta = zeros(simulation_time/h+1, 5);
rstHistory = zeros(simulation_time/h+1, 9);

RSTpara.R = [1 0 0];
RSTpara.S = [1 0 0];
RSTpara.T = [1 0 0];

designParameter.Gms_omega0 = 4;
designParameter.GMs_D = 1.05;
designParameter.dynamicFaktorAobs = 4;

adaptionfactor = 0.96;

%% RUN SIMULATION:
tic;
for k=1:simulation_time/h
    %% REFERENCE FILTER:
    TRr(k) = Gk(k, u, TRr, RSTpara.T, RSTpara.R);
    
    %% CONTROL ERROR:
    if k~=1
        e(k) = TRr(k)-f(k-1);
    else
       e(k) = TRr(k); 
    end
    
    %% LIMITATION OF PLANT INPUT
    if(abs(e(k))>20)
       e(k) = sign(e(k))*20; 
    end
    
    %% PLANT:
    [y(k), yia, ttmp] = VirtualMotor(e(k), k-1);
    
    %% FEEDBACK:
    f(k) = Gk(k, y, f, RSTpara.S, RSTpara.R);
    
    %% RECUURSIVE OBERSERVER:
    Theta(k,:) = recursiceEstimator( e(k), y(k), adaptionfactor)';
    
    %% UPDATE CONTROLLER:
    rstHistory(k,1:3)=RSTpara.R; rstHistory(k,4:6)=RSTpara.S; rstHistory(k,7:9)=RSTpara.T;
    RSTpara = adapt_controller_v4(Theta(k,:), designParameter, RSTpara);
end
toc;

%% PLOTTING CONTROLLER RESULTS:    
fig = figure;
hold on; grid on;
stairs(t, y);
stairs(t, u);
stairs(t, e);
stairs(t, TRr);
stairs(t, f);
legend('y', 'u', 'e', 'TRr', 'f');
xlabel('Time [s]')
ylabel('Amplitude')
ylim([-100 2000]);
% save_figureAsPdf('fig/controller_internal_values_overshoot.pdf');

%% PLOTTING CONTROLLER ADAPTION
figure;
hold on; grid on;
stairs(t, -rstHistory(:,3));
stairs(t, rstHistory(:,4));
stairs(t, rstHistory(:,5));
stairs(t, rstHistory(:,6));
stairs(t, rstHistory(:,7));
stairs(t, rstHistory(:,8));
stairs(t, rstHistory(:,9));
legend('r_1', 's_0', 's_1', 's_2', 't_0', 't_1', 't_2')
xlabel('Time [s]')
ylabel('Amplitude')
save_figureAsPdf('fig/controller_parameter_development.pdf')

%% PLOTTING ESTIMATOR RESULTS
figure;
hold on; grid on;
stairs(t, Theta(:,1));
stairs(t, Theta(:,2));
stairs(t, Theta(:,3));
stairs(t, Theta(:,4));
legend('a1','a2','b1','b2');
ylim([-2.5 5])
xlabel('Time [s]')
ylabel('Amplitude')
save_figureAsPdf(['fig/Estimator_forgetting_' num2str(adaptionfactor) '.pdf'])

figure;
hold on; grid on;
stairs(t, Theta(:,5));
xlabel('Time [s]')
ylabel('Estimation error [1]')
save_figureAsPdf('fig/Estimator_error.pdf')
