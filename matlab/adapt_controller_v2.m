function RSTParameter = adapt_controller_v2( sysParam )
%ADAPT_CONTROLLER Adapt an RST controller according to the current system
%parameter and the design requirements.
%   expect
%   sysParam: a1, a2, b1, b2

%% INIT: TARGET DYNAMICS
persistent Amz; persistent a1m;
persistent Bmz; persistent b1m;
if isempty(Amz)
    Bmz = [0 0.1052];
    Amz = [1 -1.105];
   a1m = Amz(2); b1m = Bmz(2);
end

a1 = sysParam(1);
a2 = sysParam(2);
b1 = sysParam(3);
b2 = sysParam(4);
%% COMPARE OF THE COEFFICIENTS OF THE CONTROLLER EQUATIONS: RST_param = A\b

A = [0 0 b1 0 0 0 0 0;
    0 0 b2 b1 0 -b1m 0 0;
    0 0 0 b2 b1 0 -b1m 0;
    0 0 0 0 b2 0 0 -b1m;
    -1 0 0 0 0 -1 0 0; ... %Denominator equations:
    1-a1 0 0 0 0 -a1m 1 0; ...
    -a2+a1 -b1 0 0 0 0 -a1m -1; ...
    a2 -b2 0 0 0 0 0 -a1m];
b = [b1m;
    0;
    0;
    0;
    1-a1-b1+a1m;
    -a2+a1-b1;
    a2-b2;
    0];

singleParameter = A\b;
RSTParameter.R = [1 -(1+singleParameter(1)) singleParameter(1)];
RSTParameter.S = [0 1 -singleParameter(2)];
RSTParameter.T = [singleParameter(3) singleParameter(4) singleParameter(5)];
end

