function RSTparameter = adapt_controller_v4( sysParam, RSTparameter)
%ADAPT_CONTROLLER Adapt an RST controller according to the current system
%parameter and the design requirements. Calculations based on the example
%of Mota.

[Amz, Bmz] = get_controllerDesign(sysParam);
a1 = sysParam(1);
a2 = sysParam(2);
b1 = sysParam(3);
b2 = sysParam(4);

am1 = Amz(2); am2 = Amz(3); bm1 = Bmz(2); bm2 = Bmz(3);
%% SOLVE RST PARAMETER:

if(det(Eq)~=0)
    rst = Eq\b;

end
end

