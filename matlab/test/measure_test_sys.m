clear all;
clc

test_sys = tf([2 1], [1 3 2]);
h=0.1
test_sysd = c2d(test_sys, h)
Aztest = cell2mat(test_sysd.Denominator);
Bztest = cell2mat(test_sysd.Numerator);
sysParamTest.Az = Aztest;
sysParamTest.Bz = Bztest;
sysParamTest.h = h;
sysParamTest.polesLaplace = pole(test_sys);

%% STEP MEASUREMENT:
[y, t] = step(test_sysd, 100);

measurement_data = zeros(1001, 4);
measurement_data(:,1) = t;
measurement_data(:,3) = y;
measurement_data(:,2) = ones(1001,1);

save('../data/test_step.mat', 'measurement_data');

%% SINUS MEASUREMENT:
u = sin(t);
[y, t] = lsim(test_sysd, u, t, 0);

measurement_data = zeros(1001, 4);
measurement_data(:,1) = t;
measurement_data(:,3) = y;
measurement_data(:,2) = u;

save('../data/test_sin.mat', 'measurement_data');

%% WHITE NOISE MEASUREMENT:
u = wgn(1001,1,1);
[y, t] = lsim(test_sysd, u, t, 0);

measurement_data = zeros(1001, 4);
measurement_data(:,1) = t;
measurement_data(:,3) = y;
measurement_data(:,2) = u;

save('../data/test_wgn.mat', 'measurement_data');