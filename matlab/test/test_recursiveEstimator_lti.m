clear all
clc
addpath('../');
%% INIT
load('../data/test_step.mat');
parameter = zeros(size(measurement_data, 1), 4);

%% ESTIMATE PARAMETERS:
for k=1:size(measurement_data, 1)
    parameter(k,:) = recursiceEstimator(measurement_data(k,2), measurement_data(k, 3));
end

%% PLOT ESTIMATOR DEVELOPMENT:
figure;
hold on;
stairs(measurement_data(:,1), parameter(:,1));
stairs(measurement_data(:,1), parameter(:,2));
stairs(measurement_data(:,1), parameter(:,3));
stairs(measurement_data(:,1), parameter(:,4));
legend('a1', 'a2', 'b1', 'b2');