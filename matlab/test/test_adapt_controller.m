%% Teste die Implementierung von Mota eines RST controllers mit den Werten aus 
% der Vorlesungsfolie
clear all;
clc
addpath('../');

%% INIT:
h = 0.1; 

%% EXERCISE SYSTEMS: 
s = tf('s');
G = 600/(s+1)/(s+3);
Gd = c2d(G,h);

Bz = cell2mat(Gd.Numerator);
b1 = Bz(2);
b2 = Bz(3);
Az = cell2mat(Gd.Denominator);
a1 = Az(2);
a2 = Az(3);

sysParam = [a1 a2 b1 b2];

designParameter.Gms_omega0 = 2;
designParameter.GMs_D = 1;
designParameter.dynamicFaktorAobs = 2;

RSTparameter.R = [1 0 0]; %to be sure that the implementation will not brake.
RSTparameter.S = [0 0 0];
RSTparameter.T = [1 0 0];
%% 
RSTparameter = adapt_controller_v4( sysParam, designParameter, RSTparameter);
disp(['My RST parameter: r1=' num2str(-RSTparameter.R(3)) ', S=[' num2str(RSTparameter.S(1)) ' ' num2str(RSTparameter.S(2)) ' ' num2str(RSTparameter.S(3)) ']'])





%% Alternative Calculation (Sascha):
theta = sysParam;
%% desired behavior
wnm=2;%frequency
xim=1;%damping
%according to page 21 Am(q)=
am1=-2*exp(-xim*wnm*h)*cos(wnm*sqrt(1-xim^2)*h);
am2=exp(-2*xim*wnm*h);


%% observer
wn_obs=4; %frequency in the s domain -> should be faster then gm
xi_obs=1;%damping factor in the s domain 
%conversion s ->z
aobs1=-2*exp(-xi_obs*wn_obs*h)*cos(wn_obs*sqrt(1-xi_obs^2)*h); 
aobs2=exp(-2*xi_obs*wn_obs*h);
%% gain new coeficients
    a1=theta(1);
    a2=theta(2);
    b1=theta(3);
    b2=theta(4);

%% adapt the controler
  % according to page 21
    bm_quote=(1+am1+am2)/(b1+b2);
  % according to page 23 
    P0=am1+aobs1+1-a1;
    P1=a2+a1*P0-am1-am2-aobs2-aobs1*(1+am1)-1;
    P2=b1*(1-a1)+b2;
    P3=a2*(P0-1)-a1*P0-am1*aobs2-am2*aobs1;
    P4=b1*(a1-a2);
    P5=-a2*P0-am2*aobs2;
    P6=-P3*P2+P4*P1;
   % caclutating new rst parameter
    s1=-(b1*(P5*P2-P1*a2*b1)+b2*P6)/(b2*(P4*b1-P2*b2)-a2*b1^3);
    s2=(P6+(P4*b1-P2*b2)*s1)/(P2*b1);
    s0=-(P1+b1*s1)/P2;
    r1=P0-b1*s0;

    disp(['RST parameter Sascha: r1=' num2str(r1) ', S=[' num2str(s0) ' ' num2str(s1) ' ' num2str(s2) ']'])
    disp(['Observer: a1m=' num2str(aobs1) ' , a2m=' num2str(aobs2)])
    disp(['bm_=' num2str(bm_quote)]);
    disp(['SystemParameter: a1=' num2str(a1) ', a2=' num2str(a2) ', b1=' num2str(b1) ', b2=' num2str(b2)])
    
    
    
    
    
    
%% Solution Ant�nio:
th = theta;
a1=th(1);
a2=th(2);
b1=th(3);
b2=th(4);

wnm=2;
qsim=1;
wn0=4;
qsi0=1;
am1=-2*exp(-qsim*wnm*h)*cos(wnm*sqrt(1-qsim^2)*h);
am2=exp(-2*qsim*wnm*h);
a01=-2*exp(-qsi0*wn0*h)*cos(wn0*sqrt(1-qsi0^2)*h);
a02=exp(-2*qsi0*wn0*h);
bmlinha=(1+am1+am2)/(b1+b2);
bm1=b1*bmlinha;
bm2=b2*bmlinha;
P0=am1+a01+1-a1;
P1=a2+a1*P0-am1-am2-a02-a01*(1+am1)-1;
P2=b1*(1-a1)+b2;
P3=a2*(P0-1)-a1*P0-am1*a02-am2*a01;
P4=b1*(a1-a2);
P5=-a2*P0-am2*a02;
P6=-P3*P2+P4*P1;
s1=-(b1*(P5*P2-P1*a2*b1)+b2*P6)/(b2*(P4*b1-P2*b2)-a2*b1^3);
s2=(P6+(P4*b1-P2*b2)*s1)/(P2*b1);
s0=-(P1+b1*s1)/P2;
r1=P0-b1*s0;

disp(['RST parameter Antonio: r1=' num2str(r1) ', S=[' num2str(s0) ' ' num2str(s1) ' ' num2str(s2) ']'])