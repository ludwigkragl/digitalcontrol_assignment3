clear all;
clc
addpath('../');

%% INIT:
h=0.1; %[s]
load('sysParamTest')
simulation_time = 100; %[s]
t = (0:h:simulation_time)';
u = zeros(simulation_time/h+1, 1);
u(1:20/h-1)=300;
u(20/h: 40/h)=900;
u(40/h: 60/h-1)=1200;
u(60/h: 80/h-1)=600;
u(80/h: 100/h)=0;
TRr = zeros(simulation_time/h+1, 1);
e = zeros(simulation_time/h+1, 1);
f = zeros(simulation_time/h+1, 1); %feedback: S/T*y
y = zeros(simulation_time/h+1, 1);
Theta = zeros(simulation_time/h+1, 5);

sysParamTest.Az = [1 -1.629281019107614 0.670320046035639];
sysParamTest.Bz = [0 0.021886113658982  0.019152913269044];

RSTparameter = adapt_controller_v3([sysParamTest.Az(2:3) sysParamTest.Bz(2:3)]);

%% RUN SIMULATION:
for k=1:simulation_time/h
    %% REFERENCE FILTER:
    TRr(k) = Gk(k, u, TRr, RSTparameter.T, RSTparameter.R);
    
    %% CONTROL ERROR:
    if k~=1
        e(k) = TRr(k)-f(k-1);
    else
       e(k) = TRr(k); 
    end
    
    %% PLANT:
    y(k) = Gk(k, e, y, sysParamTest.Bz, sysParamTest.Az);
    
    %% FEEDBACK:
    f(k) = Gk(k, y, f, RSTparameter.S, RSTparameter.R);
end

%% PLOTTING CONTROLLER RESULTS:    
figure;
hold on
stairs(t, TRr);
stairs(t, e);
stairs(t, y);
stairs(t, f);
stairs(t, u);
legend('TRr', 'e', 'y', 'f', 'u');
xlabel('Time [s]')
ylabel('Amplitude')
title('Test adapt controller')

disp(['Controller Parameter: R=[' num2str(RSTparameter.R) '], S=[' num2str(RSTparameter.S) '], T=[' num2str(RSTparameter.T) ']'])