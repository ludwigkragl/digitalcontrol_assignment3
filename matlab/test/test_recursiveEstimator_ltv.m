clear all;
clc
addpath('../');
%% INIT:
h=0.1; %[s]
simulation_time = 100; %[s]
t = (0:h:simulation_time)';
u = zeros(simulation_time/h+1, 1);
u(1:20/h-1)=300;
u(20/h: 40/h)=900;
u(40/h: 60/h-1)=1200;
u(60/h: 80/h-1)=600;
u(80/h: 100/h)=0;
TRr = zeros(simulation_time/h+1, 1);
e = zeros(simulation_time/h+1, 1);
f = zeros(simulation_time/h+1, 1); %feedback: S/T*y
y = zeros(simulation_time/h+1, 1);
Theta = zeros(simulation_time/h+1, 5);

load('controlParameter.mat'); % initial control parameter R, S, T
RSTparameter.R = [1 0];
RSTparameter.S = [0 0];
RSTparameter.T = [1 0];

designParameter.Gms_omega0 = 1;
designParameter.GMs_D = 0.707;
designParameter.dynamicFaktorAobs = 4;

adaptionfactor = 1;

%% INIT ANTONIOS ESTIMATOR:
alpha = 1000;
npar = 4;
P = eye(npar)*alpha;
Phi = zeros(npar,1);
th = 0.5*ones(npar,1);
forgFactor = 1;
eA = 0;
eAs = zeros(simulation_time/h+1, 1);
ths = zeros(simulation_time/h+1, 4);

%% RUN SIMULATION:
tic;
for k=1:simulation_time/h
    %% REFERENCE FILTER:
    TRr(k) = Gk(k, u, TRr, RSTparameter.T, RSTparameter.R);
    
    %% CONTROL ERROR:
    if k~=1
        e(k) = TRr(k)-f(k-1);
    else
       e(k) = TRr(k); 
    end
    
    %% PLANT:
    [y(k), yia, ttmp] = VirtualMotor(u(k), k-1);
    
    %% FEEDBACK:
    f(k) = Gk(k, y, f, RSTparameter.S, RSTparameter.R);
    
    %% RECUURSIVE OBERSERVER:
    Theta(k,:) = recursiceEstimator( u(k), y(k), adaptionfactor)';
    %Antonios estimator:
    [th,P,eA] = rls_cd(th,Phi,P,y(k),npar,forgFactor);
    for j = 1 : npar - 1
        Phi(npar + 1 - j) = Phi(npar - j);
    end
    Phi(1) = -y(k);
    Phi(1+npar/2) = u(k);
    eAs(k) = eA;
    ths(k,:) = th';
    
    %% UPDATE CONTROLLER:
    RSTparameter = adapt_controller_v4(Theta(k,:), designParameter, RSTparameter);
end
toc;


%% PLOTTING ESTIMATOR RESULTS
figure;
hold on
stairs(t, Theta(:,5));
stairs(t, Theta(:,1));
stairs(t, Theta(:,2));
stairs(t, Theta(:,3));
stairs(t, Theta(:,4));
legend('error', 'a1','a2','b1','b2');
title(['Forgettingfactor ' num2str(adaptionfactor)])
ylim([-5 5])
%% PLOTTING ESTIMATOR RESULTS
figure;
hold on
stairs(t, eAs);
stairs(t, ths(:,1));
stairs(t, ths(:,2));
stairs(t, ths(:,3));
stairs(t, ths(:,4));
legend('error', 'a1','a2','b1','b2');
title(['Antonio: Forgettingfactor ' num2str(forgFactor)])
ylim([-5 5])