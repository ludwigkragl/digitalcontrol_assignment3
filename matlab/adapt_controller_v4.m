function RSTparameter = adapt_controller_v4( sysParam, designParameter, RSTparameter)
%ADAPT_CONTROLLER Adapt an RST controller according to the current system
%parameter and the design requirements.
[Amz, Bmz, Aobsz] = get_controllerDesign(sysParam, designParameter);
a1 = sysParam(1);
a2 = sysParam(2);
b1 = sysParam(3);
b2 = sysParam(4);
am1 = Amz(2); am2 = Amz(3); o1=Aobsz(2); o2=Aobsz(3);

%% SOLVE R AND S POLYNOM:
Eq = [1 b1 0 0;
    a1-1 b2 b1 0;
    a2-a1 0 b2 b1;
    -a2 0 0 b2];
b = [-a1+1+am1+o1;
    -a2+a1+o2+am1*o1+am2;
    a2+am1*o2+am2*o1;
    am2*o2];
if(det(Eq)~=0)
    rs = Eq\b;
    % CALCULATE T POLYNOM: 
    bm_ = (1+am1+am2)/(b1+b2); %Bm'
    T = Aobsz*bm_;
    
    RSTparameter.R = [1 rs(1)-1 -rs(1)];
    RSTparameter.S = [rs(2) rs(3) rs(4)];
    RSTparameter.T = T;
end
end

