function yk = Gk( k, u, y, Nkoef, Dkoef)
%Gk calculates the output of the discrete transfer function f = Gk*u with
%Gk = Nkoef/Dkoef
%% ADAPT INPUTS:
order = size(Dkoef,2)-1;
u = [zeros(order, 1); u];
y = [zeros(order, 1); y];
Dkoef = fliplr(Dkoef)';
% adapt order of numerator to denominator:
emptyNkoef = zeros(order+1, 1);
emptyNkoef(1:size(Nkoef,2),1) = fliplr(Nkoef)';
Nkoef = emptyNkoef;

yk = ( sum(Nkoef.*u(k:k+order)) - sum(Dkoef(1:end-1).*y(k:k+order-1)))/Dkoef(end);
end

