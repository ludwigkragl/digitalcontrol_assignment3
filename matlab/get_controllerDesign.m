function [Amz, Bmz, Aobsz] = get_controllerDesign(sysParam, designParameter)
%GET_CONTROLLERDESIGN Calculates the current target behaviour of the
%closed loop transfer function based on the current system parameter and
%according to the approach of Mota sc2rst_2017_pt.pdf

%% INIT:
persistent pers_Amz;
persistent pers_Aobsz;
b1 = sysParam(3);
b2 = sysParam(4);

%% CALCUALATING AMZ:
if isempty(pers_Amz)
    s = tf('s');
    omega0 = designParameter.Gms_omega0;
    D = designParameter.GMs_D;
    Gms = omega0^2/(s^2+2*D*omega0*s+omega0^2);
    Gmz = c2d(Gms, 0.1);
    pers_Amz = cell2mat(Gmz.Denominator);
end
Amz = pers_Amz;
am1 = Amz(2); am2 = Amz(3);

%% CALCULATING BMZ:
bm1 = b1*(1+am1+am2)/(b1+b2); % see sc2rst_2017_pt.pdf
bm2 = b2*(1+am1+am2)/(b1+b2);
Bmz = [0 bm1 bm2];

%% CALCULATION AOBSZ
if isempty(pers_Aobsz)
    omega0Obs = omega0*designParameter.dynamicFaktorAobs;
    DObs = 1.5;
    Gobss = omega0Obs^2/(s^2+2*DObs*omega0Obs*s+omega0Obs^2);
    Gobsz = c2d(Gobss, 0.1);
    pers_Aobsz = cell2mat(Gobsz.Denominator);
end
Aobsz = pers_Aobsz;
end