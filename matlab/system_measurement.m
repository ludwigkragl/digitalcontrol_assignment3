clear all 
clc

%% EXPERIMENT CONFIGURATION:
h=0.1; %[s]
simulation_time = 100; %[s]
U = 10; %Amplitude [V]

%% CREATE SOME MEASUREMENT DATA:
measurement_data = zeros(simulation_time/h+1, 4);
u = U*ones(simulation_time/h+1, 1);
for i=1:(simulation_time/h+1)
    [w_rpm,ia,t] = VirtualMotor(u(i), i-1);
    measurement_data(i, :) = [t u(i) w_rpm ia];
end
save(['data/step' num2str(U) 'V.mat'], 'measurement_data');

%% PLOT RESULTS:
figure;
hold on;
%title(['Step ' num2str(U) ' V'])
yyaxis left;
ylim([0, ceil(max(measurement_data(:,4)))]);
ylabel('Current i_a [A]');
stairs(measurement_data(:,1), measurement_data(:,4))
yyaxis right;
grid on;
ymax = max(measurement_data(:,3));
ymax_digits = -numel(num2str(ceil(ymax)));
ylim([0, 1500]);
stairs(measurement_data(:,1), measurement_data(:,3))
legend('i_a [A]', '\omega [rpm]');
xlabel('Time [s]');
ylabel('Angular speed \omega [rpm]');
xlim([0 6])

%% SAVE FIGURE FOR LATEX:
save_figureAsPdf(['fig/step' num2str(U) 'V_detail.pdf']);