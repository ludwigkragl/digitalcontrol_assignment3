function sysParamOut = recursiceEstimator(uk, yk, adaptionfactor)
%RECURSICEESTIMATOR Estimates the system parameter dependend on system
%input output signal and the supposed system order
    %% INIT:
    n = 2; % supposed system order
    persistent Pkm1;
    persistent sysParam;
    persistent y;
    persistent u;
    persistent treshhold_error;
    if isempty(sysParam)
       sysParam = zeros(2*n, 1); 
       disp('Init var: sysParam.');
    end
    if isempty(Pkm1)
       Pkm1 = (10^5)*eye(2*n); 
       disp('Init var: Pkm1.');
    end
    if isempty(u)
       u = zeros(n+1, 1); 
       disp('Init var: u.');
    end
    if isempty(y)
       y = zeros(n+1, 1); 
       disp('Init var: y.');
    end
    if isempty(treshhold_error)
       treshhold_error = 15; 
    end
    
    %% UPDATE MEASUREMENT VEKTOR:
    y = circshift(y, 1); %shift elements by 1
    u = circshift(u, 1);
    y(1) = yk;
    u(1) = uk;
    
    %% UPDATE SYSTEM PARAMETER:
    mk = [-y(2:2+n-1)' u(2:2+n-1)']';
    estimationError = yk - mk'*sysParam;
    %ADAPT FORGETTING FACTOR (Error Dependent Forgetting Factor):
    if(estimationError<treshhold_error)
        adaptionfactor=1;
    end
    
    gk = 1/(adaptionfactor+mk'*Pkm1*mk)*Pkm1*mk;
    sysParam = sysParam + gk * ( estimationError);
    Pkm1 = 1/adaptionfactor*(eye(2*n)-gk*mk')*Pkm1; % For next calculation 
    
    %% OUT:
    sysParamOut = [sysParam; estimationError];
end