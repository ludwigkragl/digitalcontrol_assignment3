function save_figureAsPdf( file_location )
%SAVE_FIGUREASPDF Save active Figure as pdf 
%   Detailed explanation goes here

box on;
set(gcf,'color','w');   %Make it white

skalierung=1;
set(gcf ,'Units','centimeters');
width = 15*skalierung; height = 10*skalierung;
figPos = get (gcf ,'position');
figPos (3) = width ;
figPos (4) = height ;
set(gcf ,'position',figPos );

addpath('D:\Programmdateien\Matlab\Matlab-Bibliothek\FigureExport\altmany-export_fig-eaeef72');
export_fig(gcf(),file_location);
end

