clear all
clc

%% LOAD MEASUREMENT DATA
load('data/step20V.mat');

%% SETUP IDENTIFICATION:
n=2; % assumed system order
samples=4; % number of samples which shall be evaluated
sample0 = 1; %first sample that shall be evaluated

%% ADD MEASUREMENT VALUE BEFORE STEP:
measurement_data = [zeros(n, size(measurement_data, 2)); measurement_data];

%% BUILT MEASUREMENT MATRIX M:
M = zeros(samples, 2*n);
for k=1:samples
   M(k, :) = [fliplr(-measurement_data(sample0+k+n-2:sample0+k+2*n-3, 3)') ...
                fliplr(measurement_data(sample0+k+n-2:sample0+k+2*n-3, 2)')]; 
end

%% BUILD OUTPUT VECTOR y:
y = measurement_data(sample0+n+1:sample0+n+samples, 3);

%% LEAST SQUARE CALCULATION:
sys_param_opt = inv(M'*M)*M'*y
e_opt = norm(y - M*sys_param_opt, 2)