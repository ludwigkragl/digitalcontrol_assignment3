clear all
clc
%% INIT:
s = tf('s');
h=0.1;

G = 600/(s+1)/(s+3);
Gd = c2d(G,h);

Gm = 5/(s^2+4*s+5);
Gmd = c2d(Gm, h);

Bm = cell2mat(Gmd.Numerator);
bm1 = Bm(2);
bm2 = Bm(3);
Am = cell2mat(Gmd.Denominator);
am1 = Am(2);
am2 = Am(3);

B = cell2mat(Gd.Numerator);
b1 = B(2);
b2 = B(3);
A = cell2mat(Gd.Denominator);
a1 = A(2);
a2 = A(3);

%% SOLVE RST PARAMETER:

Eq = [b1 0 0 0 0 0;
    b2 b1 bm1 0 0 0;
    0 b2 bm2 0 0 0;
    0 0 1 -1 b1 0;
    0 0 -am1 a1 -b2 -b1;
    0 0 am2 -a2 0 b2];
b = [bm1; 
    bm2; 
    0; 
    -a1+am1; 
    -am2+a2; 
    0];
rst = Eq\b;
t1 = rst(1); t2 = rst(2); o1=rst(3); r1=rst(4); s1=rst(5); s2=rst(6);
%% CALC CLOSED LOOP TRANSFERFUNCTION:

TRd = tf([t1 t2],[1 -r1], h);
SRd = tf([s1 s2],[1 -r1],h);
Gd_closed_1  = TRd*Gd/(1+Gd*SRd); %6th order, but dynamics ok

% Gd_closed_2 = tf([b1*t1 b1*t2+t1*b2 t2*b2], [1 -r1+b1*s1+a1 -r1*a1+b1*s2+b2*s1 -r1*a2+b2*s2], h) % bullshit
% figure 
% step(Gd)
% figure
% step(Gmd)
% figure
% step(Gd_closed_1)

%% TRY TO SIMULATE THE NEEDED WAY
simulation_time = 100; %[s]
t = (0:h:simulation_time)';
u = zeros(simulation_time/h+1, 1);
u(1:20/h-1)=300;
u(20/h: 40/h)=900;
u(40/h: 60/h-1)=1200;
u(60/h: 80/h-1)=600;
u(80/h: 100/h)=0;
TRr = zeros(simulation_time/h+1, 1);
e = zeros(simulation_time/h+1, 1);
f = zeros(simulation_time/h+1, 1); %feedback: S/T*y
y = zeros(simulation_time/h+1, 1);
Theta = zeros(simulation_time/h+1, 5);

%load('controlParameter.mat'); % initial control parameter R, S, T
R = [1 -r1];
S = [s1 s2];
T = [t1 t2];

%% RUN SIMULATION:
for k=1:simulation_time/h
    %% REFERENCE FILTER:
    TRr(k) = Gk(k, u, TRr, T, R);
    
    %% CONTROL ERROR:
    if k~=1
        e(k) = TRr(k)-f(k-1);
    else
       e(k) = TRr(k); 
    end
    
    %% PLANT:
    y(k) = Gk(k, e, y, [0 b1 b2], [1 a1 a2]);
    
    %% FEEDBACK:
    f(k) = Gk(k, y, f, S, R);
end

%% PLOTTING CONTROLLER RESULTS:    
figure;
hold on
stairs(t, TRr);
stairs(t, e);
stairs(t, y);
stairs(t, f);
stairs(t, u);
legend('TRr', 'e', 'y', 'f', 'u');
xlabel('Time [s]')
ylabel('Amplitude')
title('Exercise')

disp(['Controller Parameter: R=[' num2str(R) '], S=[' num2str(S) '], T=[' num2str(T) ']'])